<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\TarifaProducto;
use App\Models\Calendario;
use Auth;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\Exports\ProductoExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;


class CalendarioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['calendario']=Calendario::with('producto')->where('estado',1)->where('created_by',Auth::user()->id)->get();
        $data['productos']=Producto::with(['tarifas','imagenes'])->where('estado',1)->get();
        return view('calendario')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // Validation's
        $validator=Validator::make($request->all(), [
            'fecha' => ['required', 'string'],
            'unidades' => ['required', 'numeric'],
            'total_pagar' => ['required', 'numeric'],
            'producto_id' => ['exists:producto,id'],
        ]); 

        // Si hay un error recorro los mensajes y los muestro en el Front
        $response=ValidatorController::validateAndSendMessageError($validator,'calendario');
        if($response){
            return $response;
        }

        $cita=new Calendario();
        $cita->fecha = ($request->fecha)?Carbon::parse($request->fecha)->format('Y-m-d H:i:s'):NULL;
        $cita->producto_id = $request->producto_id;
        $cita->unidades = $request->unidades;
        $cita->total_pagar = $request->total_pagar;
        $cita->created_by = Auth::user()->id;
        $cita->save();
        
        
        $response_message_back = [
          'icon' => 'success',
          'title' => 'Agenda Creada con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
        ];

        return redirect('calendario')->with('response_message_back', $response_message_back);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        // Validation's
        $validator=Validator::make($request->all(), [
            'fecha' => ['required', 'string'],
            'unidades' => ['required', 'numeric'],
            'total_pagar' => ['required', 'numeric'],
            'producto_id' => ['exists:producto,id'],
        ]); 

        // Si hay un error recorro los mensajes y los muestro en el Front
        $response=ValidatorController::validateAndSendMessageError($validator,'calendario');
        if($response){
            return $response;
        }

        $cita=Calendario::find($id);
        $cita->fecha = ($request->fecha)?Carbon::parse($request->fecha)->format('Y-m-d H:i:s'):NULL;
        $cita->producto_id = $request->producto_id;
        $cita->unidades = $request->unidades;
        $cita->total_pagar = $request->total_pagar;
        $cita->updated_by = Auth::user()->id;
        $cita->update();
        
        $response_message_back = [
          'icon' => 'success',
          'title' => 'Agenda Actualizada con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
        ];

        return redirect('calendario')->with('response_message_back', $response_message_back);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Actualizo estado a 0 que es igual a Inactivo
         $cita_calendario=Calendario::find($id);
         $cita_calendario->estado=0;
         $cita_calendario->update();

        return response()->json(['success' => true, 'message' => 'Esta Agenda Fue Eliminada Del Calendario Con Exito!']);
    }

}
