<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('calendario');
});


// Route::get('/home','HomeController@index');

Auth::routes();

Route::get('/logout', ['uses' => 'App\Http\Controllers\Auth\LoginController@logout'])->name('logout');

Route::get('/home', function () {
    return redirect('calendario');
});

// Users
Route::resource('/user', App\Http\Controllers\UserController::class );
Route::get('/user-export', 'App\Http\Controllers\UserController@export');
Route::post('/delete-user-image', 'App\Http\Controllers\UserController@deleteImage');

// Categoria Producto
Route::resource('/categoria-producto', App\Http\Controllers\CategoriaController::class );
Route::get('/categoria-export', 'App\Http\Controllers\CategoriaController@export');
// Producto 
Route::resource('/producto', App\Http\Controllers\ProductoController::class );
Route::post('/producto-datatable','App\Http\Controllers\ProductoController@datatable');
Route::post('/delete-image-producto','App\Http\Controllers\ProductoController@deleteImageProducto');
Route::get('/producto-export', 'App\Http\Controllers\ProductoController@export');
Route::get('/producto-export-pdf', 'App\Http\Controllers\ProductoController@exportPdf');
//Calendario
Route::resource('/calendario', App\Http\Controllers\CalendarioController::class);









