@extends('layouts.app')

@section('content')
    <user-list-component :users="{{ json_encode($users) }}" :asset_url_storage="'{{ asset('storage') }}'"></user-list-component>
    <user-modal-component :asset_url_storage="'{{ asset('storage') }}'" ></user-modal-component>
@endsection


<!-- Sctips's -->
@section('scripts_level')
    <!-- Daterangepicker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Plugin Fileinput -->
    <script src="{{mix('js/fileinput.js')}}" defer></script>
    <link href="{{mix('css/fileinput.css')}}" rel="stylesheet" />
    <!-- Daterangepicker -->
    <script src="{{mix('js/daterangepicker_function.js')}}"></script>
    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js" ></script>

    <script type="text/javascript">

        $(document).ready(function(){
            // Validation's
            $('#form').validate();

            // Pugin Fileinput
            $('.input_adjundtar_archivo').fileinput({
                theme: 'fa',
                language: 'es',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,  
                showUploadedThumbs: false, 
                showRemove: true,
                showPreview: true,
                dropZoneEnabled: false,
                maxFileCount: 10,
                mainClass: "input-group-lg col-md-12"
            });


            // Function para mostrar plugin de Daterangepicker en input text
            renderDaterangeGlobal('#fecha_nacimiento','','up');

        });
    </script>
@endsection

