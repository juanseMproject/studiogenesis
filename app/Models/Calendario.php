<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Calendario extends Model
{
    public $table = 'calendario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha',
        'total_pagar',
        'unidades',
        'producto_id',
    ];


    /**
     * Get Categoria Padre 
     */
    public function producto()
    {
       return $this->belongsTo(Producto::class,'producto_id');
    }

}
