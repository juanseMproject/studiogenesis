@extends('layouts.app')


@section('css_level')
<style type="text/css">
    /* Make Select2 boxes match Bootstrap3 heights: */
    .select2-selection__rendered {
      line-height: 38px !important;
  }

  .select2-selection {
      height: 38px !important;
  }
</style>
@endsection

@section('content')
<calendario-view-component :agendas="{{ json_encode($calendario) }}" :productos="{{ json_encode($productos) }}" :asset_url_storage="'{{asset('storage')}}'"></calendario-view-component>
@endsection


<!-- Sctips's -->
@section('scripts_level')
<!-- Select2 -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js" defer></script>

<!-- Moment JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js" ></script>

<script type="text/javascript">

    $(document).ready(function(){
        // Validation's
        $('#form').validate();
        $('#form_edit').validate();

        var language_select_2={
            noResults: function() {
              return "No Hay Productos a Mostrar";        
          },
          searching: function() {
              return "Buscando...";
          }
      };

      $('.sl_2_modal_create').select2({
        language:language_select_2,
        placeholder: 'Selecciona',
        width:'100%',
        height:'50px',
        dropdownParent: $('#modalCalendarioCreate'),
        });

      $('.sl_2_modal_edit').select2({
        language:language_select_2,
        placeholder: 'Selecciona',
        width:'100%',
        height:'50px',
        dropdownParent: $('#modalCalendarioEdit'),
    });

  });
</script>
@endsection

