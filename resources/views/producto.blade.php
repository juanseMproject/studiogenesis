@extends('layouts.app')


@section('css_level')
@endsection

@section('content')
    <producto-list-component :productos="{{ json_encode($productos) }}" ></producto-list-component>
    <producto-modal-create-component :categorias="{{ json_encode($categorias) }}" ></producto-modal-create-component>
    <producto-modal-edit-component  :categorias="{{ json_encode($categorias) }}"  :asset_url_storage="'{{asset('storage')}}'" ></producto-modal-edit-component>
@endsection


<!-- Sctips's -->
@section('scripts_level')
    <!-- Plugin Fileinput -->
    <script src="{{mix('js/fileinput.js')}}" defer></script>
    <link href="{{mix('css/fileinput.css')}}" rel="stylesheet" />
    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js" ></script>

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js" defer></script>

    <!-- Datepicker -->
    <script src="https://unpkg.com/vuejs-datepicker"></script>
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            // Validation's
            $('#form').validate();
            $('#form_edit_product').validate();

            // Select 2
            var language_select_2={
                noResults: function() {
                  return "No hay resultados";        
                },
                searching: function() {
                  return "Buscando...";
                }
            };

            $('.sl_2_modal_multiple_modal_create').select2({
                language:language_select_2,
                placeholder: 'Selecciona',
                width:'100%',
                multiple:true,
            });

            $('.sl_2_modal_multiple_modal_create').val('').trigger('change');
            

            $('.sl_2_modal_multiple_modal_edit').select2({
                language:{
                    noResults: function() {
                      return "No hay resultados";        
                    },
                    searching: function() {
                      return "Buscando...";
                    }
                },
                placeholder: 'Selecciona',
                width:'100%',
                multiple:true,
            });


            // Pugin Fileinput
            $('.input_adjundtar_archivo').fileinput({
                theme: 'fa',
                language: 'es',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,  
                showUploadedThumbs: false, 
                showRemove: true,
                showPreview: true,
                dropZoneEnabled: false,
                maxFileCount: 10,
                mainClass: "input-group-lg col-md-12",
                multiple:true,
            });




        });
    </script>
@endsection

