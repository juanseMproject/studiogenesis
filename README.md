# StudioGenesis

## Configuracion del Proyecto
```
npm install
```
### Luego ejecutamos 

```
npm run dev o run watch 
```

### para actualizar nuestra key aplication debemos ejecutar  

```
php artisan key:generate
```
### para limpiar y configurar el cache ejecutamos

```
php artisan config:cache
```

### Estas son las Credenciales Base de Datos
```
DB_PORT=3306
DB_DATABASE=studio
DB_USERNAME=root
DB_PASSWORD=
```

### Creamos nuestra base de datos "studio"  - Una vez creada ejecutamos el siguiente comando

```
php artisan migrate --seed
```

### Para poder utilizar el api debemos actualizar nuestros api client's token con el siguiente comando

```
php artisan passport:install 
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```


## Versionado 📌

Usamos [Sourcetre y Git](https://github.com/tu/proyecto/tags).