<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\CategoriaProducto;
use App\Models\TarifaProducto;
use App\Models\ImagenProducto;
use App\Models\Categoria;
use Auth;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\Exports\ProductoExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ProductoController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['productos']=Producto::with(['categorias','tarifas','imagenes'])->where('estado',1)->where('created_by',Auth::user()->id)->get();

        $data['categorias']=Categoria::select(['nombre','id'])->where('estado',1)->get();
        return view('producto')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation's
        $validator=Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'descripcion' => ['string', 'max:255'],
            'fecha_inicio.*' => ['required','string'],
            'categorias.*' => ['required','string','exists:categoria,id'],
            'fecha_fin.*' => ['required','string'],
            'precio.*' => ['required','string'],
        ]); 

        // Si hay un error recorro los mensajes y redirecciono al Front
        $response=ValidatorController::validateAndSendMessageError($validator,'producto');
        if($response){
            return $response;
        }

        // Create Producto
        $producto=new Producto();
        $producto->nombre = $request->nombre;
        $producto->codigo = $request->codigo;
        $producto->descripcion = $request->descripcion;
        $producto->created_by = Auth::user()->id;
        $producto->save();
        

        if (count($request->categorias)) { 

            foreach($request->categorias as $categoria_id)
            {
                // Create Categoria de Producto
                $producto_categoria=new CategoriaProducto();
                $producto_categoria->categoria_id = $categoria_id;
                $producto_categoria->producto_id = $producto->id;
                $producto_categoria->created_by = Auth::user()->id;
                $producto_categoria->save();
            }
        }

        if (count($request->fecha_inicio)) { 

            foreach($request->fecha_inicio as $index => $fecha_inicio)
            {
                // Create Tarifa
                $producto_tarifa=new TarifaProducto();
                $producto_tarifa->fecha_inicio = ($fecha_inicio)?Carbon::parse($fecha_inicio)->format('Y-m-d H:i:s'):NULL;
                $producto_tarifa->fecha_fin = ($request['fecha_fin'][$index])?Carbon::parse($request['fecha_fin'][$index])->format('Y-m-d H:i:s'):NULL;
                $producto_tarifa->precio = ($request['precio'][$index])?$request['precio'][$index]:NULL;
                $producto_tarifa->producto_id = $producto->id;
                $producto_tarifa->created_by = Auth::user()->id;
                $producto_tarifa->save();

            }
        }


        if($request->hasFile('imagenes'))
        {   

            foreach($request->file('imagenes') as $archivo)
            {
                $file=File::get($archivo);
                $nombre_archivo = $producto->id.'_'.Carbon::now()->format('Y_m_d_h_i_s_u').'.'.$archivo->getClientOriginalExtension();
                $url_definitiva='producto/'.$producto->id.'/'.$nombre_archivo;
                Storage::disk('public')->put($url_definitiva,$file,'public');

                // Create ImagenProducto
                $producto_img=new ImagenProducto();
                $producto_img->url_imagen = $url_definitiva;
                $producto_img->producto_id = $producto->id;
                $producto_img->created_by = Auth::user()->id;
                $producto_img->save();
            }

        }

        $response_message_back = [
          'icon' => 'success',
          'title' => 'Producto Creado con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
      ];

      return redirect('producto')->with('response_message_back', $response_message_back);
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation's
        $validator=Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'descripcion' => ['string', 'max:255'],
            'fecha_inicio.*' => ['required','string'],
            'categorias_edicion.*' => ['required','string','exists:categoria,id'],
            'fecha_fin.*' => ['required','string'],
            'precio.*' => ['required','string'],
        ]); 

        // Si hay un error recorro los mensajes y redirecciono al Front
        $response=ValidatorController::validateAndSendMessageError($validator,'producto');
        if($response){
            return $response;
        }

        // Update Producto
        $producto=Producto::find($id);
        $producto->nombre = $request->nombre;
        $producto->codigo = $request->codigo;
        $producto->descripcion = $request->descripcion;
        $producto->updated_by = Auth::user()->id;
        $producto->update();

        // Valido y elimino las categorias que fueron eliminadas desde el front
        $producto_categorias_anteriores=CategoriaProducto::where('producto_id',$id)->where('estado',1)->get();

        if($producto_categorias_anteriores){
            foreach($producto_categorias_anteriores as $categoria_prod_anterior){   
                if(!in_array($categoria_prod_anterior->categoria_id,$request->categorias_edicion)){
                    // Set state inactivo
                    $categoria_prod_anterior->estado=0;
                    $categoria_prod_anterior->update();
                }
            }
        }


        if(count($request->categorias_edicion)) { 
            foreach($request->categorias_edicion as $categoria_id){ 
                $producto_categoria=CategoriaProducto::where('categoria_id',$categoria_id)->where('producto_id',$id)->where('estado',1)->first();
                $exist_object=false;
                // Si np existe este producto lo creo
                if($producto_categoria){
                    // Si existo el producto lo actualizo
                    $exist_object=true;
                    $producto_categoria->updated_by = Auth::user()->id;  
                }else{  
                    $producto_categoria=new CategoriaProducto();
                    $producto_categoria->created_by = Auth::user()->id;
                }
                // Update Categoria de Producto
                $producto_categoria->categoria_id = $categoria_id;
                $producto_categoria->producto_id = $producto->id;

                if($exist_object){
                    $producto_categoria->update();                                     
                }else{
                    $producto_categoria->save();   
                }
            }
        }

        // Valido y elimino las tarifas que fueron eliminadas desde el front
        $producto_tarifas_anteriores=TarifaProducto::where('producto_id',$id)->where('estado',1)->get();
        if($producto_tarifas_anteriores){
            foreach($producto_tarifas_anteriores as $tarifa){   
                if(!in_array($tarifa->id,$request->id_tarifa_producto)){
                    // Set state inactivo
                    $tarifa->estado=0;
                    $tarifa->update();
                }
            }
        }

        if (count($request->fecha_inicio)) { 
            foreach($request->fecha_inicio as $index => $fecha_inicio){


                // Si hay un id_tarifa_producto en el actual $index entonces lo consulto y actualizo
                $exist_object=false;
                if($request->id_tarifa_producto[$index]){
                    $exist_object=true;
                    $producto_tarifa=TarifaProducto::find($request->id_tarifa_producto[$index]);
                    $producto_tarifa->updated_by = Auth::user()->id;  
                }else{
                    // Si no hay procedo a crear de nuevo
                    $producto_tarifa=new TarifaProducto();
                    $producto_tarifa->created_by = Auth::user()->id;
                }

                // Update Tarifa
                $producto_tarifa->fecha_inicio = ($fecha_inicio)?Carbon::parse($fecha_inicio)->format('Y-m-d H:i:s'):NULL;
                $producto_tarifa->fecha_fin = ($request['fecha_fin'][$index])?Carbon::parse($request['fecha_fin'][$index])->format('Y-m-d H:i:s'):NULL;
                $producto_tarifa->precio = ($request['precio'][$index])?$request['precio'][$index]:NULL;
                $producto_tarifa->producto_id = $producto->id;
                if($exist_object){
                    $producto_tarifa->update();                    
                }else{
                    $producto_tarifa->save();                    
                }
            }
        }


        if($request->hasFile('imagenes'))
        {   
            foreach($request->file('imagenes') as $archivo){
                // Save file in Storage
                $file=File::get($archivo);
                $nombre_archivo = $producto->id.'_'.Carbon::now()->format('Y_m_d_h_i_s_u').'.'.$archivo->getClientOriginalExtension();
                $url_definitiva='producto/'.$producto->id.'/'.$nombre_archivo;
                Storage::disk('public')->put($url_definitiva,$file,'public');

                // Update ImagenProducto
                $producto_img=new ImagenProducto();
                $producto_img->url_imagen = $url_definitiva;
                $producto_img->producto_id = $producto->id;
                $producto_img->created_by = Auth::user()->id;
                $producto_img->save();
            }

        }

        $response_message_back = [
          'icon' => 'success',
          'title' => 'Producto Actualizado con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
      ];

      return redirect('producto')->with('response_message_back', $response_message_back);
    }

  /**
   * Delete producto por id
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      // Actualizo estado a 0 que es igual a Inactivo
     $producto=Producto::find($id);
     $producto->estado=0;
     $producto->update();

    // Elimino registros asociados a este producto
     ImagenProducto::where('producto_id',$id)->update(['estado' => 0]);
     CategoriaProducto::where('producto_id',$id)->update(['estado' => 0]);
     TarifaProducto::where('producto_id',$id)->update(['estado' => 0]);

     return response()->json(['success' => true, 'message' => 'Producto Eliminado con Exito!']);
 }


  /**
   * 
   * Datatable of products
   * @return \Yajra\DataTables\DataTables|
   *
   */
   public function datatable(){

      $productos=Producto::with(['tarifas','imagenes','categorias'])->where('estado',1);

      return DataTables::of($productos)
      ->editColumn('acciones', function($producto) {
          return '';
      })
      ->editColumn('imagenes', function($producto) {
          $html=""; 
         if(count($producto->imagenes))
         {
             foreach($producto->imagenes as $imagen)
             {
                  $html.='<img src="'.asset('storage/'.$imagen->url_imagen).'" width="70px" height="70px" class="rounded m-2" ></img>';
             }
         }
         else
         {
               $html='<span class="badge bg-danger p-2 m-2 bold text-white rounded">No Registra</span>';
         }
         return $html."";
     })
      ->editColumn('categorias', function($producto) {
         $html="<div class='col-md-12'>"; 
         if(count($producto->categorias))
         {
             foreach($producto->categorias as $categoria_producto)
             {    
                  $categoria=Categoria::find($categoria_producto->categoria_id);
                  $html.='<span class="badge bg-success p-2 m-2 bold text-white rounded text-md-left" >'.$categoria->nombre.'</span><br>';
             }
         }
         else
         {
              $html='<span class="badge bg-danger p-2 m-2 bold text-white rounded">No Registra</span>';
         }
         return $html."</div>";
     })
      ->editColumn('tarifas', function($producto) {
         $html=""; 
         if(count($producto->tarifas))
         {
             foreach($producto->tarifas as $index => $tarifa)
             {    
                  $html.='<div class="card mb-3 " style="width: 15rem;" >
                        <div class="card-header bg-primary text-white">
                            Tarifa # '.($index+1).'
                        </div>
                        <div class="card-body">
                            - Desde: '.Carbon::parse($tarifa->fecha_inicio)->format('Y-m-d').' <br> - Hasta: '.Carbon::parse($tarifa->fecha_fin)->format('Y-m-d').'<br> - Precio:  € '.$tarifa->precio.'  <br>
                        </div>
                    </div>';
             }
         }
         else
         {
              $html='<div class="col-md-12"><span class="badge bg-danger p-2 m-2 bold text-white rounded">No Registra</span></div>';
         }

         return $html."";
     })->rawColumns(['id','acciones','imagenes','categorias','tarifas'])->make(true);

    }
    

  /**
   * 
   * Delete image of products
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   *
   */
    public function deleteImageProducto(Request $request){

        $imagen_producto=ImagenProducto::find($request->id);
        $imagen_producto->url_imagen;
        //Elimino archivo del Storage
        Storage::delete($imagen_producto->url_imagen);                

        $imagen_producto->delete();


       return response()->json(['success' => true, 'message' => 'Archivo Eliminado con Exito!']); 
    }


    /**
     * Export excel of categorias
     *
     */
    public function export() 
    {   
        return Excel::download(new ProductoExport, 'Productos_'.Carbon::now()->format('Y_m_d_h_i_s').'.xlsx');
    }



    /**
     * Export PDF of categorias
     *
     */
    public function exportPdf() 
    {   
        $data = [
            'productos' => Producto::with(['categorias','tarifas','imagenes'])->where('estado',1)->get()
        ];

        $pdf = \PDF::loadView('pdfs.producto', $data)->setPaper('a4', 'landscape');

        return $pdf->download('Productos_'.Carbon::now()->format('Y_m_d_h_i_s').'.pdf');
            
            
    }





}
