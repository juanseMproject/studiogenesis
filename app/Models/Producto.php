<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Producto extends Model
{
    public $table = 'producto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * Get tarifas 
     */
    public function tarifas()
    {
        return $this->hasMany(TarifaProducto::class, 'producto_id', 'id')->where('estado', '=', 1);
    }


    /**
     * Get users 
     */
    public function user()
    {
       return $this->belongsTo(User::class,'created_by')->where('estado', '=', 1);
    }

    /**
     * Get categorias 
     */
    public function categorias()
    {
        return $this->hasMany(CategoriaProducto::class, 'producto_id', 'id')->where('estado', '=', 1);
    }


    /**
     * Get imagenes 
     */
    public function imagenes()
    {
        return $this->hasMany(ImagenProducto::class, 'producto_id', 'id')->where('estado', '=', 1);
    }

}
