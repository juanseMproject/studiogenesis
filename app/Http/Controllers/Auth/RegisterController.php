<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'apellido' => ['required', 'string'],
            'fecha_nacimiento' => ['required', 'string'],
            'foto_perfil' => ['image'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {   

        $user=new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->apellido = $data['apellido'];
        $user->fecha_nacimiento = ($data['fecha_nacimiento'])?Carbon::parse($data['fecha_nacimiento'])->format('Y-m-d H:i:s'):NULL;
        $user->save();
        
        if($data['foto_perfil']){
            $archivo=$data['foto_perfil'];
            $file=File::get($archivo);

            $nombre_archivo = $user->id.'_'.Carbon::now()->format('Y_m_d_h_i_s').'.'.$archivo->getClientOriginalExtension();
            $url_definitiva='user/'.$user->id.'/'.$nombre_archivo;
            Storage::disk('public')->put($url_definitiva,$file,'public');

            $user->foto_perfil=$url_definitiva;
            $user->update();
        }

        return $user;
    }


    






}
