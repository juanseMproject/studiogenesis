<?php

namespace App\Exports;

use App\Models\Producto;
use App\Models\Categoria;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ProductoExport implements FromView,ShouldAutoSize, WithHeadings, WithStyles,WithEvents
{	
    private $productos;
 
    public function __construct()
    {
        $this->productos=Producto::with(['tarifas','imagenes','categorias','user' => function($q) {
            $q->where('estado',1);
        }])->get();
    }

     /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                foreach ($this->productos as $key => $prod) {
                    $row_height= (100 * ((count($prod->tarifas) > 0) ? count($prod->tarifas) : 1));
                    // AJUSTO HEIGHT
                    $event->sheet->getRowDimension($key+2)->setRowHeight( 100 * ((count($prod->tarifas) > 0) ? count($prod->tarifas) : 1) ); 
                    // AJUSTO ANCHO DE LA COLUMNA
                    $event->sheet->getColumnDimension('F')->setAutoSize(false);
                    $event->sheet->getColumnDimension('F')->setWidth(25);

                    // Recorro las imaganes del proucto y las muestro
                    if($prod->imagenes){   
                        $espacio_entre_imagenes=0;
                        foreach($prod->imagenes as $img) {
                            // // INSERTO IMAGENES
                            $drawing = new Drawing();
                            $drawing->setName('Imagen Producto');
                            $drawing->setDescription('');
                            $drawing->setPath(public_path('/storage/'.$img->url_imagen));
                            $drawing->setHeight(50);
                            $drawing->setWidth(55);
                            $drawing->setOffsetX(8);
                            $drawing->setOffsetY($espacio_entre_imagenes);
                            $drawing->setCoordinates('G'.($key+2));
                            $drawing->setWorksheet($event->sheet->getDelegate());

                            $espacio_entre_imagenes+=80;
                        }
                        $row_height_image = (80 * ((count($prod->imagenes) > 0) ? count($prod->imagenes) : 1));
                        $event->sheet->getRowDimension($key+2)->setRowHeight( ($row_height_image>$row_height) ? $row_height_image  : $row_height ); 
                    }

                }
            },
        ];
    }


    public function view(): View
    {
        return view('export.producto', [
            'productos' => $this->productos
        ]);
    }


    public function headings(): array
    {
    	return [
    		['ID', 'Nombre','Código','Descripción','Categorias','Tarifas','Fecha Creación','Estado'],
    	];
    }

     public function styles(Worksheet $sheet)
    {   
        return [
            1    => ['font' => ['bold' => true,'size' => 12]],
        ];
    }


}
