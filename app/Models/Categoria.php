<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categoria extends Model
{
    use HasFactory;

    public $table = 'categoria';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * Get Categoria Hija 
     */
    public function categorias_hijo()
    {
        return $this->hasMany(Categoria::class, 'categoria_padre_id', 'id');
    }


    /**
     * Get Categoria Padre 
     */
    public function categoria_padre()
    {
       return $this->belongsTo(Categoria::class,'categoria_padre_id');
    }

}
