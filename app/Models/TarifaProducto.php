<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class TarifaProducto extends Model
{
    public $table = 'tarifa_producto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'precio',
        'fecha_inicio',
        'fecha_fin',
    ];

}
