@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <!-- <img src="{{asset("js/images/login.png")}}" width="100px" height="100px"> -->
                 <i class="fa fa-list"></i> 

                {{ __('REGISTRO') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id="form" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}  <span class="text-danger">(*)</span></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="apellido" class="col-md-4 col-form-label text-md-right">{{ __('Apellido') }} </label>

                            <div class="col-md-6">
                                <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" autocomplete="false" autofocus>

                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}  <span class="text-danger">(*)</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}  <span class="text-danger">(*)</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}  <span class="text-danger">(*)</span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="fecha_nacimiento" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Nacimiento') }}  <span class="text-danger">(*)</span></label>

                            <div class="col-md-6">
                                <input id="fecha_nacimiento" type="text" class="form-control @error('fecha_nacimiento') is-invalid @enderror" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" required autocomplete="false" autofocus>

                                @error('fecha_nacimiento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="foto_perfil" class="col-md-4 col-form-label text-md-right">{{ __('Foto de Perfil') }}</label>

                            <div class="col-md-6 file-loading">
                                &nbsp;&nbsp;&nbsp;<input id="foto_perfil" type="file" class="form-control @error('foto_perfil') is-invalid @enderror input_adjundtar_archivo" name="foto_perfil" value="{{ old('foto_perfil') }}" autocomplete="false" accept="image/*" autofocus>

                                @error('foto_perfil')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- Sctips's -->
@section('scripts_level')
    <!-- Daterangepicker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Plugin Fileinput -->
    <script src="{{mix('js/fileinput.js')}}" defer></script>
    <link href="{{mix('css/fileinput.css')}}" rel="stylesheet" />
    <!-- Daterangepicker -->
    <script src="{{mix('js/daterangepicker_function.js')}}"></script>

    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js" ></script>

    <script type="text/javascript">

        $(document).ready(function(){

            // Validation's
            $('#form').validate();

            // Pugin Fileinput
            $('.input_adjundtar_archivo').fileinput({
                theme: 'fa',
                language: 'es',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,  
                showUploadedThumbs: false, 
                showRemove: true,
                dropZoneEnabled: false,
                maxFileCount: 10,
                mainClass: "input-group-lg"
            });

            // Function para mostrar plugin de Daterangepicker en input text
            renderDaterangeGlobal('#fecha_nacimiento','','up');
        });
    </script>
@endsection

