<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use Auth;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Exports\CategoriaExport;
use Maatwebsite\Excel\Facades\Excel;

class CategoriaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categorias']=Categoria::with('categoria_padre')->where('estado',1)->get();
        return view('categoria')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation's
        $validator=Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'codigo' => ['required', 'string', 'max:255'],
            'descripcion' => ['nullable','string', 'max:255'],
            'categoria_padre_id' => ['nullable','exists:categoria,id']
        ]); 

        // Si hay un error recorro los mensajes y los muestro en el Front
        $response=ValidatorController::validateAndSendMessageError($validator,'categoria-producto');
        if($response){
            return $response;
        }
        // Creo la Nueva Categoria
        $categoria=new Categoria();
        $categoria->nombre = $request->nombre;
        $categoria->codigo = $request->codigo;
        $categoria->descripcion = $request->descripcion;
        $categoria->categoria_padre_id = $request->categoria_padre_id;
        $categoria->created_by = Auth::user()->id;
        $categoria->save();
        
        $response_message_back = [
          'icon' => 'success',
          'title' => 'Categoría de Producto Creada con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
        ];

        return redirect('categoria-producto')->with('response_message_back', $response_message_back);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        // Validation's
        $validator=Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'codigo' => ['required', 'string', 'max:255'],
            'descripcion' => ['nullable','string', 'max:255'],
            'categoria_padre_id' => ['nullable','exists:categoria,id']
        ]); 

        // Si hay un error recorro los mensajes y los muestro en el Front
        $response=ValidatorController::validateAndSendMessageError($validator,'categoria-producto');
        if($response){
            return $response;
        }

        $categoria=Categoria::find($id);
        $categoria->nombre = $request->nombre;
        $categoria->codigo = $request->codigo;
        $categoria->descripcion = $request->descripcion;
        $categoria->categoria_padre_id = $request->categoria_padre_id;
        $categoria->created_by = Auth::user()->id;
        $categoria->update();
        
        $response_message_back = [
          'icon' => 'success',
          'title' => 'Categoría de Producto Actualizada con Exito!',
          'message' => 'La información fue guardada satisfactoriamente',
        ];

        return redirect('categoria-producto')->with('response_message_back', $response_message_back);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Actualizo estado a 0 que es igual a Inactivo
         $categoria=Categoria::find($id);
         $categoria->estado=0;
         $categoria->update();

        return response()->json(['success' => true, 'message' => 'Categoría de Producto Eliminado con Exito!']);
    }

    /**
     * Export excel of categorias
     *
     */
    public function export() 
    {   
        return Excel::download(new CategoriaExport, 'Categorias_'.Carbon::now()->format('Y_m_d_h_i_s').'.xlsx');
    }

}
