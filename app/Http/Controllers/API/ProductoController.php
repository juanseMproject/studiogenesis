<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Producto;
use Auth;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ProductoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {



        if($request->fecha){
            $fecha=Carbon::parse($request->fecha)->format('Y-m-d');
            $data['productos']=Producto::join('tarifa_producto','tarifa_producto.producto_id','producto.id')->where('tarifa_producto.fecha_inicio','<',$fecha)->where('tarifa_producto.fecha_fin','>',$fecha)->get();
        }else{   
            $data['productos']=Producto::with(['categorias','tarifas','imagenes'])->get();
        }
        return response()->json($data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }



}

