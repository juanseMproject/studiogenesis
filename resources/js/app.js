/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';

import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
import moment from 'moment';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// NavBar
Vue.component('navbar-component', require('./components/NavBarComponent.vue').default);
// User - Usuarios Components
Vue.component('user-list-component', require('./components/user/UserListComponent.vue').default);
Vue.component('user-modal-component', require('./components/user/UserModalComponent.vue').default);
// Categoria Producto Components
Vue.component('categoria-producto-list-component', require('./components/categoria_producto/CategoriaProductoListComponent.vue').default);
Vue.component('categoria-producto-modal-create-component', require('./components/categoria_producto/CategoriaProductoModalCreateComponent.vue').default);
Vue.component('categoria-producto-modal-edit-component', require('./components/categoria_producto/CategoriaProductoModalEditComponent.vue').default);
// Producto Components
Vue.component('producto-list-component', require('./components/producto/ProductoListComponent.vue').default);
Vue.component('producto-modal-create-component', require('./components/producto/ProductoModalCreateComponent.vue').default);
Vue.component('producto-modal-edit-component', require('./components/producto/ProductoModalEditComponent.vue').default);
Vue.component('producto-tarifa-section-component', require('./components/producto/ProductoTarifaSectionComponent.vue').default);
Vue.component('producto-list-card-component', require('./components/producto/ProductoListCardComponent.vue').default);
// Calendario
Vue.component('calendario-view-component', require('./components/calendario/CalendarioViewComponent.vue').default);
Vue.component('calendario-modal-create-component', require('./components/calendario/CalendarioModalCreateComponent.vue').default);
Vue.component('calendario-modal-edit-component', require('./components/calendario/CalendarioModalEditComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.Event = new Vue();

Vue.filter('formatDate', function(value) {
  if(value) {
    return moment(String(value)).format('MM/DD/YYYY');
  }
})


// Este es una variable necesaria para los Datatables
window.language_datatable ={
  'sProcessing': 'Procesando...',
  'sLengthMenu': 'Mostrar _MENU_ registros',
  'sZeroRecords': 'No se encontraron resultados',
  'sEmptyTable': 'Ningún dato disponible en esta tabla',
  'sInfo': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
  'sInfoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
  'sInfoFiltered': '(filtrado de un total de _MAX_ registros)',
  'sInfoPostFix': '',
  'sSearch': 'Buscar:',
  'sUrl': '',
  'sInfoThousands': ',',
  'sLoadingRecords': 'Cargando...',
  'oPaginate': {
      'sFirst': 'Primero',
      'sLast': 'Último',
      'sNext': 'Siguiente',
      'sPrevious': 'Anterior'
  },
  'oAria': {
      'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
      'sSortDescending': ': Activar para ordenar la columna de manera descendente'
  }
}; 


const app = new Vue({
    el: '#app',
});

