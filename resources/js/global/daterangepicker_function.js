var customFormatGlobal = 'YYYY-MM-DD hh:mm:ss A';
window.renderDaterangeGlobal = function (selector_element,idParent,drop='up')
{
	
	var option={
		singleDatePicker: true,
		timePicker: false,
		changeMonth: true,
		changeYear: true,
		drops:drop,
		locale: {
			format: customFormatGlobal,
			separator: ' - ',
			applyLabel: 'Aceptar',
			cancelLabel: 'Cancelar',
			fromLabel: 'Desde',
			toLabel: 'Hasta',
			customRangeLabel: 'Custom',
			daysOfWeek: [
			'Do',
			'Lu',
			'Ma',
			'Mi',
			'Ju',
			'Vi',
			'Sa'
			],
			monthNames: [
			'Enero',
			'Febrero',
			'Marzo',
			'Abril',
			'Mayo',
			'Junio',
			'Julio',
			'Agosto',
			'Septiembre',
			'Octubre',
			'Noviembre',
			'Diciembre'
			],
			firstDay: 1
		}
	};


	$(selector_element).daterangepicker(option);
}