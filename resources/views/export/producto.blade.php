<table>
    <thead>
        <tr>
           <th>ID</th>
           <th>Nombre</th>
           <th>Código</th>
           <th>Descripción</th>
           <th>Categorias</th>
           <th>Tarifas</th>
           <th>Imagenes</th>
           <th>Fecha Creación</th>
           <th>Creado Por</th>
           <th>Estado</th>
       </tr>
   </thead>
   <tbody>
    @foreach($productos as $prod)
    <tr>
        <td>{{ $prod->id }}</td>
        <td>{{ $prod->nombre }}</td>
        <td>{{ $prod->codigo }}</td>
        <td>{{ $prod->descripcion }}</td>
        <td align="center" style="vertical-align:center;padding:5px">
            @foreach($prod->categorias as $cat)
                - {{(\App\Models\Categoria::find($cat->categoria_id))->nombre}} <br>
            @endforeach
        </td>
        <td align="left" style="vertical-align:center;padding:5px" >
            @foreach($prod->tarifas as $index => $tar)
                 Tarifa # {{$index+1}} <br> 
                 - Desde: {{\Carbon\Carbon::parse($tar->fecha_inicio)->format("Y-m-d")}}  <br>
                 - Hasta: {{\Carbon\Carbon::parse($tar->fecha_fin)->format("Y-m-d")}} <br> 
                 - Precio:  € {{$tar->precio}} <br><br>
            @endforeach
        </td>
        <td>
            
        </td>
       <td>
            {{\Carbon\Carbon::parse($prod->created_at)->format("Y-m-d")}}
        </td>
        <td>
            {{$prod->user->name.' '.$prod->user->apellido}} 
        </td>
        <td>
            {{($prod->estado)?'Activo':'Inactivo'}}   
        </td>
    </tr>
    @endforeach
</tbody>
</table>