const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');


//Fileinput 
mix.styles(['resources/js/plugins/plugin-subir-archivos/js/fileinput.js',
			'resources/js/plugins/plugin-subir-archivos/themes/fa/theme.js',
			'resources/js/plugins/plugin-subir-archivos/js/locales/es.js'], 'public/js/fileinput.js');

mix.styles(['resources/js/plugins/plugin-subir-archivos/css/fileinput.css'],'public/css/fileinput.css');    

// Daterangepicker Function
mix.js('resources/js/global/daterangepicker_function.js', 'public/js')