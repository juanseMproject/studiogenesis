<?php

namespace App\Exports;

use App\Models\Categoria;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CategoriaExport implements FromCollection,ShouldAutoSize, WithHeadings, WithMapping, WithStyles
{	
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {	
    	return Categoria::with(['categoria_padre'])->get();
    }

    public function map($cat): array
    {	

    	return [
    		$cat->id,
    		$cat->nombre,
    		$cat->codigo,
    		$cat->descripcion,
    		($cat->categoria_padre)?$cat->categoria_padre->nombre:'No Registra',
    		Carbon::parse($cat->created_at)->format('Y-m-d'),
    		($cat->estado)?'Activo':'Inactivo',
    	];
    }

    public function headings(): array
    {
    	return [
    		['ID', 'Nombre','Código','Descripción','Categoria Padre','Fecha Creación','Estado'],
    	];
    }

     public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true,'size' => 12]],
        ];
    }


}
