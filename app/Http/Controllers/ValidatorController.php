<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ValidatorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Valida los errores del Validator, los recorre y redirecciona al Front
     * @param  Illuminate\Support\Facades\Validator  $validator
     * @param  String  $url_redirect
     * @return \Illuminate\Http\Response
     */
    public static function  validateAndSendMessageError($validator,$url_redirect)
    {
     	// Si hay un error recorro los mensajes y los muestro en el Front
        if($validator->fails()) {
            $string_errores='';
            foreach ($errores=json_decode($validator->errors()) as $key => $value_object) {
                foreach ($value_object as $key => $mensaje_error) {
                    $string_errores.=' - '.$mensaje_error.'\n';
                }
            }
            $response_message_back = [
              'icon' => 'error',
              'title' => ''.$string_errores,
              'message' => '',
            ];
            return redirect($url_redirect)
                        ->with('response_message_back',$response_message_back)
                        ->withInput();
        }

        return null;
    }

}
