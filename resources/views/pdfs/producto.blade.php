<html>
<head>
    
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Helvetica;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #3490dc;
            color: white;
            text-align: center;
            line-height: 30px;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 35px;
            background-color: #3490dc;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        .table {
            border: #dfdfdf 2px solid;
            border-radius: 10px;
        }

        .table_header {
            background: #f7f7f7;
            font-size: 18px;
            font-weight: bold;
            border-radius: 15px;
        }

    </style>
</head>
<body>
    <header>
        <h1>Listado de Productos</h1>
    </header>

    <main>

        <table style="width:100%" class="table">
            <thead class="table_header">
                <tr>
                    <th >ID</th>
                    <th > Nombre </th>
                    <th > Código </th>
                    <th > Descripción </th>
                    <th > Categorias</th>
                    <th width="180px"> Tarifas </th>
                    <th> Fecha Creación </th>
                    <th width="150px"> Creado Por </th>
                    <th>Estado</th>
                 </tr>
             </thead>
             <tbody>
                @foreach($productos as $prod)
                <tr>
                    <td>{{ $prod->id }}</td>
                    <td>{{ $prod->nombre }}</td>
                    <td>{{ $prod->codigo }}</td>
                    <td>{{ $prod->descripcion }}</td>
                    <td align="center" style="vertical-align:center;padding:5px">
                        @foreach($prod->categorias as $cat)
                        - {{(\App\Models\Categoria::find($cat->categoria_id))->nombre}} <br>
                        @endforeach
                    </td>
                    <td align="left" style="vertical-align:center;padding:5px" >
                        @foreach($prod->tarifas as $index => $tar)
                        Tarifa # {{$index+1}} <br> 
                        - Desde: {{\Carbon\Carbon::parse($tar->fecha_inicio)->format("Y-m-d")}}  <br>
                        - Hasta: {{\Carbon\Carbon::parse($tar->fecha_fin)->format("Y-m-d")}} <br> 
                        - Precio:  € {{$tar->precio}} <br><br>
                        @endforeach
                    </td>
                    <td>
                        {{\Carbon\Carbon::parse($prod->created_at)->format("Y-m-d")}}
                    </td>
                    <td>
                        {{$prod->user->name.' '.$prod->user->apellido}} 
                    </td>
                    <td>
                        {{($prod->estado)?'Activo':'Inactivo'}}   
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</main>

<script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>

<footer>
    <span>Studio Genesis - Todos los Derechos Reservados</span>
</footer>

</body>
</html>