<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fecha');
            $table->integer('unidades');
            $table->decimal('total_pagar');
            $table->unsignedInteger('producto_id');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->integer('estado')->default(1);
            $table->timestamps();

            $table->foreign('producto_id')->references('id')->on('producto');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('calendario', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);     
        });
        Schema::dropIfExists('calendario');
    }
}
