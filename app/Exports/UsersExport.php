<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class UsersExport implements FromCollection,ShouldAutoSize, WithHeadings, WithMapping, WithStyles, WithDrawings, WithEvents, WithCustomStartCell
{	
    private $users;
 
    public function __construct()
    {
        $this->users=User::all();
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                foreach ($this->users as $key => $user) {
                    if($user->foto_perfil){
                        // AJUSTO HEIGHT
                        $event->sheet->getRowDimension($key+2)->setRowHeight(80); 
                        // AJUSTO ANCHO DE LA COLUMNA
                        $event->sheet->getColumnDimension('E')->setAutoSize(false);
                        $event->sheet->getColumnDimension('E')->setWidth(32);

                        // INSERTO IMAGENES
                        $drawing = new Drawing();
                        $drawing->setName('Imagen Pefil');
                        $drawing->setDescription('');
                        $drawing->setPath(public_path('/storage/'.$user->foto_perfil));
                        $drawing->setHeight(50);
                        $drawing->setWidth(55);
                        $drawing->setCoordinates('E'.($key+2));
                        $drawing->setWorksheet($event->sheet->getDelegate());

                    }
                }
            },
        ];
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
    	return $this->users;
    }

    public function map($user): array
    {   
    	return [
    		$user->id,
    		$user->name,
    		$user->apellido,
    		$user->email,
            '',
    		Carbon::parse($user->fecha_nacimiento)->format('Y-m-d'),
    		Carbon::parse($user->created_at)->format('Y-m-d'),
    		($user->estado)?'Activo':'Inactivo',
    	];
    }


    public function startCell(): string
    {
        return 'A1';
    }

    public function headings(): array
    {
    	return [
    		['ID', 'Nombre','Apellido','Email','Foto','Fecha Nacimiento','Fecha Creación','Estado'],
    	];
    }


    public function drawings()
    {      
        return [];
    }


    public function styles(Worksheet $sheet)
    {   
        return [
            1    => ['font' => ['bold' => true,'size' => 12]],
        ];
    }


}
