@extends('layouts.app')


@section('css_level')
<style type="text/css">
    /* Make Select2 boxes match Bootstrap3 heights: */
    .select2-selection__rendered {
      line-height: 38px !important;
  }

  .select2-selection {
      height: 38px !important;
  }
</style>
@endsection

@section('content')
    <categoria-producto-list-component :categorias="{{ json_encode($categorias) }}"></categoria-producto-list-component>
    <categoria-producto-modal-create-component :categorias="{{ json_encode($categorias) }}" ></categoria-producto-modal-create-component>
    <categoria-producto-modal-edit-component :categorias="{{ json_encode($categorias) }}" ></categoria-producto-modal-edit-component>
@endsection


<!-- Sctips's -->
@section('scripts_level')
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js" defer></script>

    <script type="text/javascript">

        $(document).ready(function(){
            // Validation's
            $('#form').validate();
            $('#form_edit').validate();

            var language_select_2={
                noResults: function() {
                  return "No hay resultados";        
                },
                searching: function() {
                  return "Buscando...";
                }
            };

            $('.sl_2_modal_create').select2({
                language:language_select_2,
                placeholder: 'Selecciona',
                width:'100%',
                height:'50px',
                dropdownParent: $('#modalCategoriaProductoCreate'),
            });

            $('.sl_2_modal_edit').select2({
                language:language_select_2,
                placeholder: 'Selecciona',
                width:'100%',
                height:'50px',
                dropdownParent: $('#modalCategoriaProductoEdit'),
            });

        });
    </script>
@endsection

