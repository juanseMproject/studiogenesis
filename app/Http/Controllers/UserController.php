<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data['users']=User::where('estado',1)->get();

        return view('users')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        // Validation's
        $validator=Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,{$id}'],
            'apellido' => ['required', 'string'],
            'fecha_nacimiento' => ['required', 'string'],
            'foto_perfil' => ['image'],
        ]);

        $response=ValidatorController::validateAndSendMessageError($validator,'user');
        if($response){
            return $response;
        }


        $user=User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->apellido = $request->apellido;
        $user->fecha_nacimiento = ($request->fecha_nacimiento)?Carbon::parse($request->fecha_nacimiento)->format('Y-m-d H:i:s'):NULL;
        
        if($request->foto_perfil){

            if($user->foto_perfil){
                // Elimino la antigua ruta del archivo
                Storage::delete($user->foto_perfil);                
            }

            $archivo=$request->file('foto_perfil');
            $file=File::get($archivo);
            $nombre_archivo = $user->id.'_'.Carbon::now()->format('Y_m_d_h_i_s').'.'.$archivo->getClientOriginalExtension();
            $url_definitiva='user/'.$user->id.'/'.$nombre_archivo;
            Storage::disk('public')->put($url_definitiva,$file,'public');

            $user->foto_perfil=$url_definitiva;
        }


        $user->update();


        $response_message_back = [
          'icon' => 'success',
          'title' => 'Usuario Actualizado con Exito!',
          'message' => 'La información fue actualizada satisfactoriamente',
        ];


        return redirect('user')->with('response_message_back', $response_message_back);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Actualizo estado a 0 que es igual a Inactivo
         $user=User::find($id);
         $user->estado=0;
         $user->update();

        return response()->json(['success' => true, 'message' => 'Usuario Eliminado con Exito!']);
    }

    /**
     * Export excel of users
     *
     */
    public function export() 
    {   
        return Excel::download(new UsersExport, 'Usuarios_'.Carbon::now()->format('Y_m_d_h_i_s').'.xlsx');
    }


      /**
   * 
   * Delete image of user
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   *
   */
    public function deleteImage(Request $request){

        $user=User::find($request->id);
        $user->url_imagen;
        //Elimino archivo del Storage
        Storage::delete($user->foto_perfil);               
        $user->save();


       return response()->json(['success' => true, 'message' => 'Archivo Eliminado con Exito!']); 
    }

}
