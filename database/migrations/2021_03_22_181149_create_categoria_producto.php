<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriaProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_producto', function (Blueprint $table) {
            $table->id();
            $table->string('categoria_id')->nullable();
            $table->string('producto_id')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->integer('estado')->default(1);
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categoria');
            $table->foreign('producto_id')->references('id')->on('categoria');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categoria_producto', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['categoria_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);     
        });

        Schema::dropIfExists('categoria_producto');
    }
}
