/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************************************!*\
  !*** ./resources/js/global/daterangepicker_function.js ***!
  \*********************************************************/
var customFormatGlobal = 'YYYY-MM-DD hh:mm:ss A';

window.renderDaterangeGlobal = function (selector_element, idParent) {
  var drop = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'up';
  var option = {
    singleDatePicker: true,
    timePicker: false,
    changeMonth: true,
    changeYear: true,
    drops: drop,
    locale: {
      format: customFormatGlobal,
      separator: ' - ',
      applyLabel: 'Aceptar',
      cancelLabel: 'Cancelar',
      fromLabel: 'Desde',
      toLabel: 'Hasta',
      customRangeLabel: 'Custom',
      daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      firstDay: 1
    }
  };
  $(selector_element).daterangepicker(option);
};
/******/ })()
;