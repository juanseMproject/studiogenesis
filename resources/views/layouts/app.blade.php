<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Studio Genesis</title>

    <!-- Jquery  -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Jquery Validations -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/localization/messages_es.min.js" defer></script>
    <!-- Sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            //Global Configuration ajax csrf_token 
            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });
        });
    </script>
    @yield('css_level')

</head>
<body>
    <div id="app">
        <navbar-component 
        :user="{{ json_encode(Auth::user()) }}" 
        :url_asset_storage="'{{ asset('storage/') }}'" 
        ></navbar-component>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

<footer class="footer">
  <div class="copyright text-center">
      <p style="margin-top: 1rem;">Un producto hecho con amor en Colombia <i class="fa fa-heart text-danger"></i></p>
  </div><!-- .copyright -->
</footer>

@yield('scripts_level')

<!-- Validacion para mensajes que vengan desde el Back -->
@if(session('response_message_back'))
<script type="text/javascript">
    Swal.fire({
      title: '{{session("response_message_back")["title"]}}',
      text:'{{session("response_message_back")["message"]}}',
      icon:'{{session("response_message_back")["icon"]}}',
      showCancelButton: false,
  });
</script>
<!-- Borra el mensaje -->
{{session()->forget('response_message_back')}}
@endif


</html>
